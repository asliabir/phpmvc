<?php
/*
 * Info: App Core Class
 * Author: Abiruzzaman Molla
 * Date: 22 April 2018
 * Create URL & loads core controllers
 * URL FORMAT - /controller/method/params
 */

class Core{
    protected $currentController = 'Pages';
    protected $currentMethod = 'index';
    protected $params = [];

    public function __construct(){
        $this->getUrl();
    }
    public function getUrl(){
        if (isset($_GET['url'])){
            echo $_GET['url'];
        }
    }
}
?>